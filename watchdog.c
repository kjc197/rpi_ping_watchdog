/**
 * This file is part of the Kris Chaplin's personal Raspberry Pi project.
 *
 * Copyright (C) 2012 Kris Chaplin <kristian.chaplin@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 */

/**
 * watchdog.c
 * Arms the system Watchdog and then creates two threads:
 *
 * (1) watchdogHeartbeat
 *     This thread keeps the watchdog happy with periodic writes to the
 *     interface, but only if shared variable "pingCountdown" has not reached
 *     zero.  On each iteration through the heartbeat loop, pingCountdown is
 *     decremented by 1.  It is expected that the checkAlive function
 *     periodically resets the value to a large number every time the system
 *     is deemed by the checkAlive thread to be working normally.
 * (2) checkAlive
 *     This thread runs an external command to check that the board is still
 *     alive. If the external program  has a return code of 0, the system is
 *     deemed stable, and the value of the pingCountdown timer is reset.  If
 *     non-zero, pingCountdown remains unchanged, and will eventually expire
 *      via watchdogHeartbeat, causing the Watchdog to reset the Linux system.
 *
 * This program is expected to "never" exit.  The Watchdog may cause a reset,
 * if the maximum time is exceeded with no response from checkAlive, however
 * in normal operating conditions, the watchdog will keep ticking along
 * ad-infinitum.
 *
 */

#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <semaphore.h>
#include <linux/watchdog.h>
#include <errno.h>
#include <stdlib.h>

/* PING_DELAY is the minimum time to wait in seconds by checkAlive in the event
 * that the system is operating normally */
#define PING_DELAY 60

/* WATCHDOG_TOGGLE_DELAY is the minumum delay in seconds between touches to the
 * Watchdog timer to prevent the system being reset */
#define WATCHDOG_TOGGLE_DELAY 2

/* PING_FAILURE_MULTIPLE is the number of PING_DELAY cycles to wait before the
 * failure to respond to the external system alive script is deemed fatal */
#define PING_FAILURE_MULTIPLE 5

/* WATCHDOGDEV is the watchdog device in the /dev file system - usually
 * /dev/watchdog */
#define WATCHDOGDEV "/dev/watchdog"

/* CHECK_SCRIPT is the script that is periodically called to check if the host
 * is satisfied that it is up and alive */
#define CHECK_SCRIPT "check_alive.sh"

#define COUNTDOWN_RESET_VALUE (PING_DELAY*PING_FAILURE_MULTIPLE) / \
    WATCHDOG_TOGGLE_DELAY

int fd;
int pingCountdown = COUNTDOWN_RESET_VALUE;

/* Mutex to protect pingCountdown */
sem_t pingCountdownMutex;

/* setPingVal
 *
 * Sets the pingCountdown global variable to the value in the argument
 * after locking the mutex
 *
 * @param value to set pingCountdown to
 * @return void
 *
 */

void setPingVal(int value)
{
    sem_wait(&pingCountdownMutex);
    pingCountdown = value;
    sem_post(&pingCountdownMutex);
}

/* checkAlive
 *
 * Execute the defined check script and reset the pingVal count if the check
 * succeeds
 *
 * @param no inputs
 * @return never returns
 *
 */

void * checkAlive()
{
    int ret;
    for(;; )
    {
        ret=system(CHECK_SCRIPT);
        if (ret == 0)
        {
            setPingVal(COUNTDOWN_RESET_VALUE);
            sleep(PING_DELAY);
        } else {
            fprintf(stderr,"Error - host not up!\n");
        }
    }
}

/* getPingVal
 *
 * Locks the mutex, and reads the current value of pingCountdown. If the
 * countdown has not reached zero, decrement it by one. Read and decrement are
 * kept atomic by policy of using mutexes.
 *
 * @param No inputs
 * @return Value of the pingCountdown variable
 */
int getPingVal()
{
    int value;

    sem_wait(&pingCountdownMutex);
    value = pingCountdown;
    if (pingCountdown != 0) {
        pingCountdown--;
    }
    sem_post(&pingCountdownMutex);
    return value;
}

/* watchdogHeartbeat
 *
 *
 * @param no inputs
 * @return never returns
 *
 */

void * watchdogHeartbeat()
{
    int value;
    for(;; )
    {
        value = getPingVal();
        /* printf("Consumer read %d\n",value); */
        if (value != 0) {
            /* printf("Ping WDT\n"); */
            ioctl(fd, WDIOC_KEEPALIVE, NULL);
        } else {
            system("sync");
            fprintf(stderr,
                    "Test Failed, no longer toggling WDT!\n");
            fprintf(stderr," Prepare to reboot!\n");
        }
        sleep(WATCHDOG_TOGGLE_DELAY);
    }
}

/* main
 *
 * The main function opens the watchdog timer (arming it), and then spawns two
 * processes, one to keep resetting the watchdog, the other to check that the
 * system is up and happy.
 *
 * @param no inputs
 * @return never returns
 */

int main()
{
    pthread_t ptid,ctid;
    char *dev;
    int interval;
    dev=WATCHDOGDEV;
    interval=15;
    int status;

    /* Initialize the Mutex to a 1 (available) */

    sem_init(&pingCountdownMutex,0,1);

    /* Warining: Once the watchdog device file is open, the watchdog will be
       activated by the driver */
    fd = open(dev, O_RDWR);

    if (-1 == fd) {
        fprintf(stderr, "Error: %s\n", strerror(errno));
        exit(1);
    }

    status = ioctl(fd, WDIOC_SETTIMEOUT, &interval);

    if (status != 0) {
        fprintf(stderr,
                "Error: Set watchdog interval failed with value %d\n",
                status);
        /* Let's not quit here, as the watchdog is armed! */
    }

    /* Bop the watchdog */
    ioctl(fd, WDIOC_KEEPALIVE, NULL);

    /* Create the two threads checkAlive and watchdogHeartBeat */

    if(pthread_create(&ptid, NULL,checkAlive, NULL))
    {
        fprintf(stderr,"\n Error: creating checkAlive thread");
        exit(1);
    }
    /* Bop the watchdog */
    ioctl(fd, WDIOC_KEEPALIVE, NULL);

    if(pthread_create(&ctid, NULL,watchdogHeartbeat, NULL))
    {
        fprintf(stderr,"\n Error: creating watchdogHeartbeat");
        exit(1);
    }
    /* Bop the watchdog */
    ioctl(fd, WDIOC_KEEPALIVE, NULL);

    /* Software should never get past this point, as neither checkAlive nor
     * watchdogHeartbeat finish.  If the following code is run, nothing is
     * keeping alive the watchdog, so things will start to go wrong pretty
     * quickly from here onwards.
     */

    /* Wait until the checkAlive process completes */
    if(pthread_join(ptid, NULL))
        fprintf(stderr,"\n Error: joining thread");

    fprintf(stderr,
            "\n Critical!: Watchdog is still running, function exiting");
    fprintf(stderr,
            "\n Sync'ing ready for the watchdog to kick us into reset");

    /* Sync may help maintain the file system - we're about to die */
    system("sync");

    /* Wait until the watchdogHeartbeat process completes */

    if(pthread_join(ctid, NULL))
        fprintf(stderr,"\n Error: joining thread");

    sem_destroy(&pingCountdownMutex);

    /* exit the main thread */
    pthread_exit(NULL);
    return 1;
}
