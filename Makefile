CC=gcc
CFLAGS = -O3 -lpthread
OBJECTS = watchdog.o

watchdog : $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o watchdog

%.o : %.c
	$(CC) $(CFLAGS) -c $<

clean :
	rm -f *.o watchdog *unc-backup*
pretty:
	uncrustify -c ~/uncrustify.cfg -f watchdog.c -o watchdog.c
