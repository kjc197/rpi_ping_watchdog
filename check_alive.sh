#!/bin/bash

# This file is part of the Kris Chaplin's personal Raspberry Pi project.
# 
# Copyright (C) 2012 Kris Chaplin <kristian.chaplin@gmail.com>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# Check Alive script.  This Script will check the host IP defined in the HOSTS
# variable, and run a ping test the number of times 

# HOSTS variable specifies the host name or IP address to check with a ping
HOSTS="192.168.1.254"

# COUNT is the number of unique pings to try
COUNT=4

for myHost in $HOSTS
do
  # Grab the %received number output from ping
  count=$(ping -c $COUNT $myHost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')
  if [ $count -eq 0 ]; then
    # If 0%, then we have total failure from the pings, host not reachable
    echo "Host : $myHost is down (ping failed) at $(date)"
    # Host is down
    exit 1;
  fi
done

# Host is up
exit 0;
